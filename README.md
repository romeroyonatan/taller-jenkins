# jenkins-dind
> Jenkins con cliente docker
# Build

```bash
docker build . -t taller-jenkins
```

# Ejecución
```bash
docker run -v /var/run/docker.sock:/var/run/docker.sock \
           -p 8080:8080 \
           -d \
           taller-jenkins
```
