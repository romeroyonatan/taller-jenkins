from .truco import Carta, Mazo, Palo


__all__ = ["Carta", "Mazo", "Palo"]
