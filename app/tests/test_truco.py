import random
import unittest

from truco import Carta, Mazo, Palo


class TrucoTests(unittest.TestCase):
    def test_ancho_de_espada_es_mayor(self):
        ancho_espadas = Carta(1, Palo.ESPADA)
        ancho_basto = Carta(1, Palo.BASTO)
        siete_espada = Carta(1, Palo.BASTO)
        siete_oro = Carta(1, Palo.BASTO)

        self.assertGreater(ancho_espadas, ancho_basto)
        self.assertGreater(ancho_espadas, siete_espada)
        self.assertGreater(ancho_espadas, siete_oro)

    def test_ordenar_mazo_de_mayor_a_menor(self):
        mazo = Mazo()
        # sorted ordena de menor a mayor por defecto, por eso requiere el
        # parametro reverse
        mazo_ordenado = sorted(mazo, reverse=True)

        self.assertEqual(mazo_ordenado[0], Carta(1, Palo.ESPADA))
        self.assertEqual(mazo_ordenado[-1].numero, 4)

    def test_mazo_se_puede_mezcar_con_libreria_builtin(self):
        mazo = Mazo()

        # no debe lanzar ninguna excepcion
        random.shuffle(mazo)

        self.assertTrue(isinstance(mazo[0], Carta))

    def test_puntos_envido(self):
        carta1 = Carta(6, Palo.BASTO)
        carta2 = Carta(7, Palo.BASTO)

        # no debe lanzar ninguna excepcion
        self.assertEqual(carta1.puntos_envido(carta2), 33)
        self.assertEqual(carta2.puntos_envido(carta1), 33)

    def test_puntos_envido_con_una_figura(self):
        carta1 = Carta(6, Palo.BASTO)
        carta2 = Carta(10, Palo.BASTO)

        # no debe lanzar ninguna excepcion
        self.assertEqual(carta1.puntos_envido(carta2), 26)
        self.assertEqual(carta2.puntos_envido(carta1), 26)

    def test_puntos_envido_con_dos_figuras(self):
        carta1 = Carta(12, Palo.BASTO)
        carta2 = Carta(10, Palo.BASTO)

        # no debe lanzar ninguna excepcion
        self.assertEqual(carta1.puntos_envido(carta2), 20)
        self.assertEqual(carta2.puntos_envido(carta1), 20)

    def test_no_hay_envido(self):
        carta1 = Carta(6, Palo.ORO)
        carta2 = Carta(7, Palo.BASTO)

        # no debe lanzar ninguna excepcion
        self.assertEqual(carta1.puntos_envido(carta2), 0)
        self.assertEqual(carta2.puntos_envido(carta1), 0)


if __name__ == '__main__':
    unittest.main()
