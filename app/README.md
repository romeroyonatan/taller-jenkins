# Truco argentino

Mini truco argentino que sirve como ejemplo para practicar integración continua


## Requisitos

* Python 3.7
* Sphinyx (para generar la documentación)

(En la imagen de docker ya esta todo resuelto, no hay que instalar nada)

## Para ejecutar los tests

```
python -m unittest
```

## Para generar la documentación

```
cd doc
make html
```

El resultado de la documentación estará en `doc/_build/html`
