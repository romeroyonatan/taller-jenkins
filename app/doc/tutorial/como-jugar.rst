¿Cómo jugar al truco?
======================

Se Juega con las barajas españolas::

    class Palo(enum.Enum):
        BASTO = "BASTO"
        COPA = "COPA"
        ESPADA = "ESPADA"
        ORO = "ORO"


Jerarquía de cartas
--------------------

Estas son las cartas mas poderosas del juego::

    las_pulentas = {
        Carta(1, Palo.ESPADA): 14,
        Carta(1, Palo.BASTO): 13,
        Carta(7, Palo.ESPADA): 12,
        Carta(7, Palo.ORO): 11,
    }


Esta es la escala de peso en el juego, no importa el palo, sino solo el numero
(por eso las pulentas son especiales)::


    pesos = {
        3: 10,
        2: 9,
        1: 8,
        12: 7,
        11: 6,
        10: 5,
        7: 4,
        6: 3,
        5: 2,
        4: 1,
    }


Truco
------
Se juegan 3 rondas, el que gana 2 de tres gana el juego. Se comparan el "peso"
de cada carta::

    >>> from truco import Carta, Palo
    >>> ancho_espada = Carta(1, Palo.ESPADA)
    >>> siete_oro = Carta(7, Palo.ORO)
    >>> ancho_espada > siete_oro
    True


Envido
-------
Si tengo 2 cartas del mismo palo suman 20. Ademas, si tengo 4, 5, 6 o 7 se suman
los numeros de carta. Si tengo 10, 11 y 12 suma 0::

    def puntos_envido(self, other):
        """Suma de puntos de envido entre las dos cartas"""
        if self.palo != other.palo:
            return 0
        return 20 + self.tantos + other.tantos

    @property
    def tantos(self):
        """Devuelve los puntos para envido que suma esta carta."""
        if self.numero in (10, 11, 12):
            return 0
        return self.numero


Ejemplo
^^^^^^^^
::

    >>> ancho_basto = Carta(1, Palo.BASTO)
    >>> siete_oro = Carta(7, Palo.ORO)
    >>> seis_oro = Carta(6, Palo.ORO)
    >>> siete_oro.puntos_envido(seis_oro)
    33
    >>> siete_oro.puntos_envido(ancho_basto)
    0
    >>> seis_oro.puntos_envido(ancho_basto)
    0
    >>> seis_oro.puntos_envido(siete_oro)
    33
