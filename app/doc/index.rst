.. Truco documentation master file, created by
   sphinx-quickstart on Sun Jun 16 15:55:34 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenida/o a la documentación de truco
=========================================

.. toctree::
    :maxdepth: 2
    :caption: Contenido:

    tutorial/como-jugar
    api

.. automodule:: truco
