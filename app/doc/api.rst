API Reference
==============

Si vas a desarrollar algo con mi módulo de truco, seguro te interesará ver esto


.. autoclass:: truco.Palo
    :members:
    :special-members:
    :no-undoc-members:

.. autoclass:: truco.Carta
    :members:
    :special-members:
    :no-undoc-members:

.. autoclass:: truco.Mazo
    :members:
    :special-members:
    :no-undoc-members:
