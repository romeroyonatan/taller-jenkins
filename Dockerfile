FROM jenkins/jenkins:lts-alpine

USER root
RUN apk update && \
    apk add docker git
USER jenkins

ADD app /app/

VOLUME /var/run/docker.sock
